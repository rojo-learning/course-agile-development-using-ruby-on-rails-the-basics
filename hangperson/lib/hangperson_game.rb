class HangpersonGame
  attr_reader :word

  class << self
    # Get a word from remote "random word" service
    def get_random_word
      require 'uri'
      require 'net/http'
      uri = URI('http://watchout4snakes.com/wo4snakes/Random/RandomWord')
      Net::HTTP.post_form(uri ,{}).body
    end
  end

  def initialize(word=nil)
    @word = word.downcase || self.class.get_random_word.downcase
    @rights = ''
    @wrongs = ''
  end

  def rights
    @rights
  end

  alias_method :guesses, :rights

  def wrongs
    @wrongs
  end

  alias_method :wrong_guesses, :wrongs

  def guess(char)
    if word.include? validate(char).downcase
      add_to_rights char
    else
      add_to_wrongs char
    end
  end

  def word_with_guesses
    word.gsub chars_to_hide, '-'
  end

  def check_win_or_lose
    if failed_attempts < 7
      word_with_guesses.eql?(word) ? :win : :play
    else
      :lose
    end
  end

  private

    def chars_to_hide
       rights.empty? ? /./ : /[^#{rights}]/
    end

    def add_to_rights(char)
      unless rights.downcase.include? char.downcase
        rights << char
      else
        false
      end
    end

    def add_to_wrongs(char)
      unless wrongs.downcase.include? char.downcase
        wrongs << char
      else
        false
      end
    end

    def failed_attempts
      wrongs.length
    end

    def validate(char)
      raise ArgumentError if char.nil? or char.empty? or char=~(/[^a-z]/i)
      char
    end

end
