
def first_letter(input)
  input.nil? ? '' : input[0]
end
