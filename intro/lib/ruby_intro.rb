# When done, submit this entire file to the autograder.

# Part 1

def sum(array)
  array.reduce 0, :+
end

def max_2_sum(array)
  array.sort.tap { |array| return (array[-1] || 0) + (array[-2] || 0) }
end

def sum_to_n?(array, n)
  array.combination(2).map { |pair| pair.reduce(:+) }.include? n
end

# Part 2

def hello(name)
  'Hello, ' << name
end

def starts_with_consonant?(string)
  string =~ /\A[bcdfghjklmnpqrstvwxyz]/i
end

def binary_multiple_of_4?(string)
  multiple_of_4 = /00\z|\A0\z/
  valid_binary  = /\A[01]+\z/
  string =~ multiple_of_4 and string =~ valid_binary
end

# Part 3

class BookInStock
  attr_accessor :isbn, :price

  def initialize(isbn, price)
    @isbn  = validate_isbn(isbn)
    @price = validate_price(price)
  end

  def price_as_string
    format('$%.2f', price)
  end

  def validate_isbn(isbn)
    raise ArgumentError if isbn.empty?
    isbn
  end

  def validate_price(price)
    raise ArgumentError if price <= 0
    price
  end
end
