# CS169.1x Agile Development Using Ruby on Rails - The Basics #

This repository contains solutions to the assignments of the [BerkeleyX CS169.1x 1T2017SP][0]
course held at the edX platform.

## Projects / Assignatures

* **Ruby Intro**: TDD guided Ruby katas.

* **Hangperson Game**: Sinatra based game, guided with BDD and TDD.

---
Contains parts of code based on the original work of Armando Fox and David
Patterson and is used and distributed under the Attribution Non-Commercial 3.0
CC License.

[0]: https://www.edx.org/course/agile-development-using-ruby-rails-uc-berkeleyx-cs169-1x-0
