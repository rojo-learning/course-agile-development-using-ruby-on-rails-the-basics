# app/models/concerns/user_settings_for_movies.rb

module MoviesControllerConcern
  extend ActiveSupport::Concern

   # Turns a ratings array into a hash alike to one provided by a form
  def ratings_hash(ratings)
    {}.tap { |hash| ratings.each { |rating| hash[rating] = true } }
  end

  def set_preferences(session, sort_by, ratings)
    session[:sort_by] = sort_by
    session[:ratings] = ratings
  end

  def clear_preferences(session)
    session.delete :sort_by
    session.delete :ratings
  end
end
