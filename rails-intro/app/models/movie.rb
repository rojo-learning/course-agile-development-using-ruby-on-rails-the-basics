class Movie < ActiveRecord::Base
  class << self
    def ratings
      select(:rating).distinct.map(&:rating)
    end
  end
end
